module.exports = {
    extends: 'eslint:recommended',
    plugins: ['import', 'react', 'jest'],
    rules: {
        'linebreak-style': 'off',
        'no-unused-vars': [2, { varsIgnorePattern: '^h$' }],
        'react/jsx-uses-vars': 2,
        'comma-dangle': 0,
        'indent': ["error", 4],
        "quotes": ["warn","double"],
        "semi": ["warn", "never"]
    },
    parser: 'babel-eslint',
    parserOptions: {
        allowImportExportEverywhere: true,
        ecmaFeatures: {
            jsx: true,
        },
    },
    env: {
        browser: true,
        jest: true,
    },
};
