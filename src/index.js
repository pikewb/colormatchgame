import {app, h} from "hyperapp"
import colors from "./colors"
import primaryColors from "./PrimaryColors"

import {ntc} from "./ntc"
ntc.init()
const rgbToHex = num => num.toString(16).padStart(2, "0")

const addColors = (colorList) => {
    const r = Math.ceil(colorList.reduce((total, curVal) =>  (total + curVal.r), 0)/colorList.length)
    const g = Math.ceil(colorList.reduce((total, curVal) =>  (total + curVal.g), 0)/colorList.length)
    const b = Math.ceil(colorList.reduce((total, curVal) =>  (total + curVal.b), 0)/colorList.length)
    return {
        name: ntc.name(rgbToHex(r)+rgbToHex(g)+rgbToHex(b))[1],
            r, g, b,
            rgb: `rgb(${r}, ${g}, ${b})`
}}
const changeColor = (state, [color, subtract, idx]) => {
    const clickedColors = subtract && state.clickedColors.length > 1 ? [...state.clickedColors.filter((_, index) => idx !== index)] : [...state.clickedColors, color]
    return {
        ...state,
        currentColor: addColors(clickedColors),
        clickedColors
    }
}
const changeTarget = (state, hexCode) => {
    return {
    ...state,
        targetColor: hexCode
    }
}

const colorBox = (color, idx) => h("div", {style: {display: "grid", width: "60px", paddingTop: "5px"}}, [
    h("div", {style: {backgroundColor: color.rgb, height: "40px", border: "1px solid black"},
        onclick: [changeColor, [color, true, idx]]
    }),
    h("div", {}, color.name),
])
app({
    init: {
        clickedColors: [primaryColors.filter(it => it.name === "White")[0]],
        currentColor: primaryColors.filter(it => it.name === "White")[0],
        targetColor: "#000000"
    },
    view: state => h("div", {},
        h("div", {style: {display: "grid", gridTemplateColumns:"1fr 1fr", width: "400px"}}, [
            h("div", {}, [
                h("div", {style: {backgroundColor: state.currentColor.rgb, height: "40px"}}),
                h("div", {}, `Current Color: ${state.currentColor.name}`)
            ]),
            h("div", {style: {display: "grid", width: "40px", gridTemplateRows: "40px 20px"}}, [
                h("div", {style: {backgroundColor: `${state.targetColor}`}}),
                h("select", {onchange:[changeTarget, (e) => e.target.value]},
                    ntc.names.sort((a,b) => a[1]> b[1] ? 1 : a[1] < b[1] ? -1 : 0)
                    .map(([hex, colorName]) => h("option", {value: `#${hex}`, }, colorName)))
            ])
        ]),
        h("div", {style: {display: "grid", height: "60px", gridTemplateColumns: "repeat(6, 60px)", gap: "5px"}},
            primaryColors.map(color => h("div", {style: {backgroundColor: color.rgb, border: "1px solid black"},
            onclick: [changeColor, [color, false]]
            }))
        ),
        h("div", {style: {display: "grid", gridTemplateColumns: "repeat(5, 60px)", gap: "5px"}},
            state.clickedColors.map((color, idx) => colorBox(color, idx))
        )
    ),
    node: document.getElementById("app")
})
