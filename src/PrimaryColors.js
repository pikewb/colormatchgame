export default [["Red", 255, 0, 0], ["Blue", 0, 0, 255], ["Yellow", 255, 255, 0], ["Green", 0,255,0], ["Black", 0, 0, 0], ["White", 255, 255, 255]]
    .map(it => ({
        name: it[0],
        r: it[1],
        g: it[2],
        b: it[3],
        rgb: `rgb(${it[1]}, ${it[2]}, ${it[3]})`
    }))

